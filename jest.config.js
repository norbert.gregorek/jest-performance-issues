module.exports = {
    transform: {
        '^.+\\.(js|jsx|ts|tsx|mjs)$': "babel-jest",
        '^.+\\.(js|jsx|ts|tsx|mjs)$': "ts-jest"
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    transformIgnorePatterns: [],
    testEnvironment: "node"
}